﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ModDataSync;

namespace DataS
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        ModDataSync.datasyncEntities dbs = new ModDataSync.datasyncEntities();
        DatasyncModel.DatasyncEntities db = new DatasyncModel.DatasyncEntities();

        private void button1_Click(object sender, EventArgs e)
        {

            var name = textBox1.Text;
            var age = textBox2.Text;

            InfoStudent IStudet = new InfoStudent()
            {
                age = Convert.ToInt32(age),
                name = name
            };
            dbs.InfoStudents.Add(IStudet);
            dbs.SaveChanges();

        }
        public List<DatasyncModel.Student> GetAllStudentPostRegSql()
        {
            return db.Students.ToList();
        }
        public List<InfoStudent> GetAllStudentSql()
        {
            //return new List<InfoStudent>();
            return dbs.InfoStudents.ToList();
        }
        private void button3_Click(object sender, EventArgs e)
        {
            this.dataGridView1.DataSource = dbs.InfoStudents.ToList();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.dataGridView2.DataSource = db.Students.ToList();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            button2.Enabled = false;
            var unposteddata = dbs.InfoStudents.Where(x => x.IsPosted == false);
            // Postregsql database context 
            foreach (var item in unposteddata)
            {
                DatasyncModel.Student data = new DatasyncModel.Student()
                {
                    Age = item.age,
                    Id = item.id,
                    Name = item.name
                };
                db.Students.AddObject(data);
                db.SaveChanges();
                item.IsPosted = true;
                dbs.InfoStudents.Attach(item);
                dbs.SaveChanges();
            }
            button2.Enabled = true;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.dataGridView2.DataSource = GetAllStudentPostRegSql();
            this.dataGridView1.DataSource = GetAllStudentSql();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            var items = dbs.InfoStudents.ToList();
            foreach (var item in items)
            {
                dbs.InfoStudents.Remove(item);
                dbs.SaveChanges();
            }

            DeletePostregsql();
        }
        public void DeletePostregsql()
        {
            var items = db.Students.ToList();
            foreach (var item in items)
            {
                db.Students.DeleteObject(item);
                db.SaveChanges();
            }
        }
  
    }
}
