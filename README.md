Description of the Project
==========================

In this project we are performing the **Synchronization** of the SQL Server Database with the PostgreSQL Database. We are using the **Database First Appraoch**, so we first
created the databases of both the versions we are using as I provided the scripts of both the databases in the repository. Futhermore we are using the **Entity Framework** to make
the models of the Databases. This Framework allows us to only make the model of the SQL Server database. The PostgreSQL option is not given by default. <br />
For the PostgreSQL connection, we have to used any third party tool, so here in this Project we used the **Devart DotConnect for PostgreSQL** which allow us to establish a
connection with the PostgreSQL database. Our primary objective here is to synchronize the data from the SQL Server to the PostgreSQL.
This project contains the two main folders:

1. DataS
2. ModDataSync

**DataS** contains the Devart Entity Model of the PostgreSQL Database and the Windows Form Applicaiton on which the Synchronization is Performed. As well as the connection strings
where both the connection to the PostgreSQL and the SQL Server is maintained. <br />
**ModDataSync** contains the Entity Model of the SQL Server Database. <br />

Now here we provide some of the testing we performed on this project. Like the pushing of data to the SQL Server and then Synchronize it with the PostgreSQL. It also enables the
deletion of any object, If we delete any data from SQL Server, it can automatically be deleted from the PostgreSQL. As we told before this synchronization is based on the
**Windows Form Application** so all of the code is wrapped in a block of several button.

## Pushing Data to SQL Server
```
InfoStudent IStudet = new InfoStudent()
{
    age = Convert.ToInt32(age),
    name = name
};
dbs.InfoStudents.Add(IStudet);
dbs.SaveChanges();
```

## Synchronization Code 
```
var unposteddata = dbs.InfoStudents.Where(x => x.IsPosted == false);
// Postregsql database context 
foreach (var item in unposteddata)
{
    DatasyncModel.Student data = new DatasyncModel.Student()
    {
        Age = item.age,
        Id = item.id,
        Name = item.name
    };
    db.Students.AddObject(data);
    db.SaveChanges();
    item.IsPosted = true;
    dbs.InfoStudents.Attach(item);
    dbs.SaveChanges();
}
```

## Deletion of Data
```
var items = dbs.InfoStudents.ToList();
foreach (var item in items)
{
    dbs.InfoStudents.Remove(item);
    dbs.SaveChanges();
}

DeletePostregsql();
        
public void DeletePostregsql()
{
    var items = db.Students.ToList();
    foreach (var item in items)
    {
        db.Students.DeleteObject(item);
        db.SaveChanges();
    }
}
```

![picture](Synchronization-of-Data.JPG)

If you want complete explanation of every bit of the Project and step-by-step guide, you can refer to the documentation link uploaded in **WIKI**.